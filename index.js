const fs = require("fs");
const axios = require("axios").default;
const FormData = require("form-data");
const http = require("http");
const prompt = require("prompt");
const colors = require("colors/safe");

var log = [];

function printLog() {
  return new Promise((resolve) => {
    log.forEach((ele, i) => {
      console.log(i, ele);
    });
    resolve("resolved");
  });
}

function getSkin() {
  return new Promise((resolve) => {
    var skinId = Math.floor(Math.random() * (1310000 - 30000 + 1) + 30000);
    const hostname = "https://mcskindb.com";
    const path = `/${skinId}`;
    const subpath = `/download`;

    axios({
      method: "get",
      url: hostname + path + subpath,
      responseType: "stream",
    })
      .then((response) => {
        response.data.pipe(fs.createWriteStream("skin.png"));
        log.push(
          "✅ Downloaded random skin to 'skin.png'\n  (From: " +
            hostname +
            path +
            ")"
        );
        writeLastSkin(hostname + path);
        resolve("resolved");
      })
      .catch((err) => {
        console.log("❌ Error 1!", err);
      });
  });
}

function checkToken() {
  return new Promise((resolve, reject) => {
    if (fs.existsSync("token.json")) {
      AUTH_TOKEN = JSON.parse(fs.readFileSync("token.json"));
      log.push("✅ Found token");
      //axios.defaults.headers.common["Authorization"] = "Bearer " + AUTH_TOKEN;
      resolve(AUTH_TOKEN);
      return AUTH_TOKEN;
    } else {
      console.log("⏺ Token doesn't exist. Please authenticate...");
      reject("failed");
      //authenticate();
    }
  }).catch(authenticate);
}

function authenticate() {
  return new Promise((resolve) => {
    prompt.message = colors.cyan("[Input]");
    prompt.delimiter = colors.cyan(": ");
    prompt.start();

    prompt.get(
      {
        properties: {
          email: {
            message:
              "This will be your migrated username which is an email address",
            description: colors.green("Your Mojgang email"),
            required: true,
          },
          password: {
            hidden: true,
            replace: '*',
            description: colors.green("Your password"),
            required: true,
          }
        },
      },
      function (err, result) {
        if (err) {
          console.log("❌ Error 3!", err);
        }

        let data = {
          agent: {
            name: "Minecraft",
            version: 1,
          },
          username: result.email,
          password: result.password,
          requestUser: true,
        };
        axios
          .post("https://authserver.mojang.com/authenticate", data)
          .then((response) => {
            fs.writeFileSync(
              "token.json",
              JSON.stringify(response.data.accessToken)
            );

            fs.existsSync(
              "token.json",
              console.log("✅ Successfully updated token!\n")
            );
            checkToken(); // hmmm
            resolve(response.data.accessToken);
          })
          .catch((err) => {
            reject("❌ Error updating token!");
            console.log("❌ Error updating token!", err);
          });
      }
    );
  });
}
/**
 Record our new skin to disk, url for viewing
 */
function writeLastSkin(url) {
  fs.writeFile("lastskin.json",JSON.stringify(url), function (err) {
    //if (err) return console.log(err);
    log.push("✅ Written last skin URL to lastskin.json",`[${url}]`)
  })
}

function setSkin(AUTH_TOKEN, local) {
  return new Promise((resolve, reject) => {
    const hostname = "https://api.minecraftservices.com";
    const path = "/minecraft/profile/skins";
    let filePath = __dirname + "/skin.png";

    if (local) {
      let files = fs.readdirSync(__dirname + "/skins");
      filePath = "skins/" + files[Math.floor(Math.random() * files.length)];
    }

    log.push(`🟦 Path: ${filePath}`);
  
    fs.readFile(filePath, (err, imageData) => {
      if (err) {
        throw err;
      }
      const form = new FormData();

      // required
      form.append("variant", "classic");
      // headers
      form.append("file", imageData, {
        filepath: filePath,
        contentType: "image/png",
      });

      const headers = Object.assign(
        {
          Accept: "application/json",
          Authorization: "Bearer " + AUTH_TOKEN,
        },
        form.getHeaders()
      );

      axios
        .post(hostname + path, form, {
          headers: headers,
        })
        .then((response) => {
          log.push(
            `✅ ${!local ? "Successfully downloaded and" : "Successfully"} applied skin`
          );
          resolve("resolved");
        })
        .catch((err) => {
          switch (parseInt(err.response.status)) {
            case 401:
              console.log(colors.yellow("Token has expired!\r\n"))
              authenticate();
              break;

            default:
              break;
          }
          console.log("❌ Error 2!", err);
          console.log(`${err.response.status}: ${err.response.statusText}`);
        });
    });
  });
}

function getLocalInput() {
  console.log(colors.green("Use local /skins/ folder? [Y/N]"));
  return new Promise((resolve, reject) => {
    prompt.message = colors.cyan("[Input]");
    prompt.delimiter = colors.cyan(": ");
    prompt.start();

    prompt.get(
      {
        local: {
          message: colors.green("Use local /skins/ folder? [Y/N]"),
          description: colors.green("Use local /skins/ folder? [Y/N]"),
          type: "boolean",
        },
      },
      function (err, results) {
        let result = (results.question === 'y') ? true : false
        resolve(result);
        return result;
      }
    );
  });
}

async function main() {
  let AUTH_TOKEN = await checkToken();
  let localBool = await getLocalInput();
  if (!localBool)
    await getSkin(false);
  await setSkin(AUTH_TOKEN,localBool);
  await printLog();
}

main();
